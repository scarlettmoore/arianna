# Copyright (C) 2023 This file is copyright:
# This file is distributed under the same license as the arianna package.
# SPDX-FileCopyrightText: 2023 Vit Pelcak <vit@pelcak.org>
#
msgid ""
msgstr ""
"Project-Id-Version: arianna\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2025-01-17 00:39+0000\n"
"PO-Revision-Date: 2023-11-02 13:41+0100\n"
"Last-Translator: Vit Pelcak <vit@pelcak.org>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 23.08.2\n"

#: content/ui/BookDetailsPage.qml:16
#, kde-format
msgctxt "@info:title"
msgid "Book Details"
msgstr "Podrobnosti knihy"

#: content/ui/BookDetailsPage.qml:26
#, kde-format
msgid "Author:"
msgstr "Autor:"

#: content/ui/BookDetailsPage.qml:38
#, kde-format
msgid "Description:"
msgstr "Popis:"

#: content/ui/BookDetailsPage.qml:50
#, kde-format
msgid "Publisher:"
msgstr "Vydavatel:"

#: content/ui/BookDetailsPage.qml:62
#, kde-format
msgid "Language:"
msgstr "Jazyk:"

#: content/ui/BookDetailsPage.qml:74
#, kde-format
msgid "Publishing date:"
msgstr "Datum publikování:"

#: content/ui/BookDetailsPage.qml:85
#, kde-format
msgid "Copyright:"
msgstr "Autorská práva:"

#: content/ui/ColorSchemeDelegate.qml:13
#, kde-format
msgid "Color theme"
msgstr "Barevný motiv"

#: content/ui/EpubViewerPage.qml:79 content/ui/EpubViewerPage.qml:280
#: content/ui/EpubViewerPage.qml:284
#, kde-format
msgid "Loading"
msgstr "Načítání"

#: content/ui/EpubViewerPage.qml:81 content/ui/main.qml:167
#, kde-format
msgid "No search results"
msgstr "Žádné výsledky hledání"

#: content/ui/EpubViewerPage.qml:125
#, kde-format
msgctxt "@action:intoolbar"
msgid "Search"
msgstr "Hledat"

#: content/ui/EpubViewerPage.qml:131
#, kde-format
msgid "Book Details"
msgstr "Podrobnosti knihy"

#: content/ui/EpubViewerPage.qml:154
#, kde-format
msgid "No book selected"
msgstr "Nebyla vybrána žádná kniha"

#: content/ui/EpubViewerPage.qml:157
#, kde-format
msgid "Open file"
msgstr "Otevřít soubor"

#: content/ui/EpubViewerPage.qml:178
#, kde-format
msgid "Please choose a file"
msgstr "Prosím, vyberte soubor"

#: content/ui/EpubViewerPage.qml:179 content/ui/main.qml:364
#, kde-format
msgctxt "Name filter for EPUB files"
msgid "eBook files (*.epub *.cb* *.fb2 *.fb2zip)"
msgstr "Soubory eBook (*.epub *.cb* *.fb2 *.fb2zip)"

#: content/ui/EpubViewerPage.qml:228
#, kde-format
msgid "Copy"
msgstr "Kopírovat"

#: content/ui/EpubViewerPage.qml:234
#, kde-format
msgid "Find"
msgstr "Najít"

#: content/ui/EpubViewerPage.qml:259
#, kde-format
msgctxt "Book reading progress"
msgid "%1%"
msgstr "%1%"

#: content/ui/EpubViewerPage.qml:279
#, kde-format
msgid "Time left in chapter:"
msgstr "Zbývající čas v kapitole:"

#: content/ui/EpubViewerPage.qml:283
#, kde-format
msgid "Time left in book:"
msgstr "Zbývající čas v knize:"

#: content/ui/EpubViewerPage.qml:294
#, kde-format
msgid "Previous Page"
msgstr "Předchozí strana"

#: content/ui/EpubViewerPage.qml:320
#, kde-format
msgid "Next Page"
msgstr "Následující strana"

#: content/ui/EpubViewerPage.qml:355
#, kde-format
msgid "Loc. %s of %s"
msgstr ""

#: content/ui/EpubViewerPage.qml:356
#, kde-format
msgid "Page %s of %s"
msgstr ""

#: content/ui/EpubViewerPage.qml:357
#, kde-format
msgid "Page %s"
msgstr ""

#: content/ui/EpubViewerPage.qml:358
#, kde-format
msgid "Close"
msgstr "Zavřít"

#: content/ui/EpubViewerPage.qml:360
#, kde-format
msgid "Footnote"
msgstr "Poznámka pod čarou"

#: content/ui/EpubViewerPage.qml:361
#, kde-format
msgid "Go to Footnote"
msgstr ""

#: content/ui/EpubViewerPage.qml:362
#, kde-format
msgid "Endnote"
msgstr "Poznámka na konci"

#: content/ui/EpubViewerPage.qml:363
#, kde-format
msgid "Go to Endnote"
msgstr ""

#: content/ui/EpubViewerPage.qml:364
#, kde-format
msgid "Note"
msgstr "Poznámka"

#: content/ui/EpubViewerPage.qml:365
#, kde-format
msgid "Go to Note"
msgstr ""

#: content/ui/EpubViewerPage.qml:366
#, kde-format
msgid "Definition"
msgstr "Definice"

#: content/ui/EpubViewerPage.qml:367
#, kde-format
msgid "Go to Definition"
msgstr "Přejít na definici"

#: content/ui/EpubViewerPage.qml:368
#, kde-format
msgid "Bibliography"
msgstr "Seznam použité literatury"

#: content/ui/EpubViewerPage.qml:369
#, kde-format
msgid "Go to Bibliography"
msgstr ""

#: content/ui/GridBrowserDelegate.qml:146
#, kde-format
msgctxt "should be keep short, inside a label. Will be in uppercase"
msgid "New"
msgstr "Nový"

#: content/ui/LibraryPage.qml:19
#, kde-format
msgid "Library"
msgstr "Knihovna"

#: content/ui/LibraryPage.qml:98
#, kde-format
msgctxt "@action:inmenu"
msgid "Book Details"
msgstr ""

#: content/ui/LibraryPage.qml:110
#, kde-format
msgctxt "@info placeholder"
msgid "Add some books"
msgstr "Přidejte nějaké knihy"

#: content/ui/main.qml:18 content/ui/main.qml:302 main.cpp:65
#, kde-format
msgid "Arianna"
msgstr "Arianna"

#: content/ui/main.qml:203
#, kde-format
msgctxt "Switch to the listing page showing the most recently read books"
msgid "Home"
msgstr "Domů"

#: content/ui/main.qml:207
#, kde-format
msgid "Home"
msgstr "Domů"

#: content/ui/main.qml:210
#, kde-format
msgctxt "Switch to the listing page showing the most recently discovered books"
msgid "Recently Added Books"
msgstr "Nedávno přidané knihy"

#: content/ui/main.qml:216
#, kde-format
msgctxt ""
"Open a book from somewhere on disk (uses the open dialog, or a drilldown on "
"touch devices)"
msgid "Open Other..."
msgstr "Otevřít jinou..."

#: content/ui/main.qml:224
#, kde-format
msgctxt "Open the settings page"
msgid "Settings"
msgstr "Nastavení"

#: content/ui/main.qml:233
#, kde-format
msgctxt ""
"Heading for switching to listing page showing items grouped by some "
"properties"
msgid "Group By"
msgstr "Seskupit podle"

#: content/ui/main.qml:236
#, kde-format
msgctxt "Switch to the listing page showing items grouped by author"
msgid "Author"
msgstr "Autor"

#: content/ui/main.qml:242
#, kde-format
msgctxt "Switch to the listing page showing items grouped by series"
msgid "Series"
msgstr "Série"

#: content/ui/main.qml:244
#, kde-format
msgctxt "Title of the page with books grouped by what series they are in"
msgid "Group by Series"
msgstr "Seskupit podle seriálu"

#: content/ui/main.qml:248
#, kde-format
msgctxt "Switch to the listing page showing items grouped by publisher"
msgid "Publisher"
msgstr "Nakladatel"

#: content/ui/main.qml:254
#, kde-format
msgctxt "Switch to the listing page showing items grouped by genres"
msgid "Keywords"
msgstr "Klíčová slova"

#: content/ui/main.qml:256
#, kde-format
msgctxt "Title of the page with books grouped by genres"
msgid "Group by Genres"
msgstr "Seskupit podle žánru"

#: content/ui/main.qml:334 content/ui/SettingsPage.qml:16
#, kde-format
msgid "Settings"
msgstr "Nastavení"

#: content/ui/main.qml:342
#, kde-format
msgctxt "@action:button"
msgid "Add Books…"
msgstr ""

#: content/ui/main.qml:363
#, kde-format
msgid "Please choose files"
msgstr ""

#: content/ui/SettingsPage.qml:19
#, kde-format
msgid "Appearance"
msgstr "Vzhled"

#: content/ui/SettingsPage.qml:24
#, kde-format
msgid "Maximum width:"
msgstr "Maximální šířka:"

#: content/ui/SettingsPage.qml:38
#, kde-format
msgid "Margin:"
msgstr "Okraj:"

#: content/ui/SettingsPage.qml:53
#, kde-format
msgid "Show progress"
msgstr "Zobrazit průběh"

#: content/ui/SettingsPage.qml:64
#, kde-format
msgid "Font"
msgstr "Písmo"

#: content/ui/SettingsPage.qml:72 content/ui/SettingsPage.qml:194
#, kde-format
msgid "Change default font"
msgstr "Změnit výchozí písmo"

#: content/ui/SettingsPage.qml:77
#, kde-format
msgid "Use publisher font"
msgstr "Použít písmo publikujícího"

#: content/ui/SettingsPage.qml:88
#, kde-format
msgid "Text flow"
msgstr ""

#: content/ui/SettingsPage.qml:94
#, kde-format
msgid "Justify text"
msgstr "Zarovnat text do bloku"

#: content/ui/SettingsPage.qml:107
#, kde-format
msgid "Hyphenate text"
msgstr ""

#: content/ui/SettingsPage.qml:119
#, kde-format
msgid "Line height:"
msgstr "Tloušťka čáry:"

#: content/ui/SettingsPage.qml:136
#, kde-format
msgid "Brightness:"
msgstr "Jas:"

#: content/ui/SettingsPage.qml:154
#, kde-format
msgid "Colors"
msgstr "Barvy"

#: content/ui/SettingsPage.qml:159
#, kde-format
msgid "Invert colors"
msgstr "Invertovat barvy"

#: content/ui/SettingsPage.qml:177
#, kde-format
msgid "About Arianna"
msgstr "O aplikaci Arianna"

#: content/ui/SettingsPage.qml:185
#, kde-format
msgid "About KDE"
msgstr "O prostředí KDE"

#: content/ui/TableOfContentDrawer.qml:23
#, kde-format
msgctxt "@info:tooltip"
msgid "Open table of contents"
msgstr ""

#: content/ui/TableOfContentDrawer.qml:24
#, kde-format
msgctxt "@info:tooltip"
msgid "Close table of contents"
msgstr ""

#: content/ui/TableOfContentDrawer.qml:45
#, kde-format
msgctxt "@info:title"
msgid "Table of Contents"
msgstr "Obsah"

#: main.cpp:67
#, kde-format
msgid "EPub Reader"
msgstr "Čtečka Epub"

#: main.cpp:69
#, kde-format
msgid "2022 Niccolò Venerandi <niccolo@venerandi.com>"
msgstr "2022 Niccolò Venerandi <niccolo@venerandi.com>"

#: main.cpp:70
#, kde-format
msgid "Niccolò Venerandi"
msgstr "Niccolò Venerandi"

#: main.cpp:70 main.cpp:71
#, kde-format
msgid "Maintainer"
msgstr "Správce"

#: main.cpp:71
#, kde-format
msgid "Carl Schwan"
msgstr "Carl Schwan"

#: main.cpp:72
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Vít Pelčák"

#: main.cpp:72
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "vit@pelcak.org"

#: main.cpp:86
#, kde-format
msgid "Epub reader"
msgstr "Čtečka Epub"

#: main.cpp:87
#, kde-format
msgid "Epub file to open"
msgstr "Soubor epub k otevření"
