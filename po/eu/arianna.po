# Translation for arianna.po to Euskara/Basque (eu).
# Copyright (C) 2023-2025 This file is copyright:
# This file is distributed under the same license as the arianna package.
# SPDX-FileCopyrightText: 2024, 2025 KDE euskaratzeko proiektuko arduraduna <xalba@ni.eus>
#
# Translators:
# Iñigo Salvador Azurmendi <xalba@ni.eus>, 2023, 2024, 2025.
msgid ""
msgstr ""
"Project-Id-Version: arianna\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2025-01-17 00:39+0000\n"
"PO-Revision-Date: 2025-01-17 06:05+0100\n"
"Last-Translator: Iñigo Salvador Azurmendi <xalba@ni.eus>\n"
"Language-Team: Basque <kde-i18n-eu@kde.org>\n"
"Language: eu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 24.12.1\n"

#: content/ui/BookDetailsPage.qml:16
#, kde-format
msgctxt "@info:title"
msgid "Book Details"
msgstr "Liburuaren xehetasunak"

#: content/ui/BookDetailsPage.qml:26
#, kde-format
msgid "Author:"
msgstr "Egilea:"

#: content/ui/BookDetailsPage.qml:38
#, kde-format
msgid "Description:"
msgstr "Azalpena:"

#: content/ui/BookDetailsPage.qml:50
#, kde-format
msgid "Publisher:"
msgstr "Argitaratzailea:"

#: content/ui/BookDetailsPage.qml:62
#, kde-format
msgid "Language:"
msgstr "Hizkuntza:"

#: content/ui/BookDetailsPage.qml:74
#, kde-format
msgid "Publishing date:"
msgstr "Argitalpen data:"

#: content/ui/BookDetailsPage.qml:85
#, kde-format
msgid "Copyright:"
msgstr "Copyright:"

#: content/ui/ColorSchemeDelegate.qml:13
#, kde-format
msgid "Color theme"
msgstr "Kolore-gaia"

#: content/ui/EpubViewerPage.qml:79 content/ui/EpubViewerPage.qml:280
#: content/ui/EpubViewerPage.qml:284
#, kde-format
msgid "Loading"
msgstr "Zamatzen"

#: content/ui/EpubViewerPage.qml:81 content/ui/main.qml:167
#, kde-format
msgid "No search results"
msgstr "Ez dago bilaketaren emaitzarik"

#: content/ui/EpubViewerPage.qml:125
#, kde-format
msgctxt "@action:intoolbar"
msgid "Search"
msgstr "Bilatu"

#: content/ui/EpubViewerPage.qml:131
#, kde-format
msgid "Book Details"
msgstr "Liburuaren xehetasunak"

#: content/ui/EpubViewerPage.qml:154
#, kde-format
msgid "No book selected"
msgstr "Ez da libururik hautatu"

#: content/ui/EpubViewerPage.qml:157
#, kde-format
msgid "Open file"
msgstr "Ireki fitxategia"

#: content/ui/EpubViewerPage.qml:178
#, kde-format
msgid "Please choose a file"
msgstr "Mesedez aukeratu fitxategi bat"

#: content/ui/EpubViewerPage.qml:179 content/ui/main.qml:364
#, kde-format
msgctxt "Name filter for EPUB files"
msgid "eBook files (*.epub *.cb* *.fb2 *.fb2zip)"
msgstr "eLiburuki fitxategiak (*.epub *.cb* *.fb2 *.fb2zip)"

#: content/ui/EpubViewerPage.qml:228
#, kde-format
msgid "Copy"
msgstr "Kopiatu"

#: content/ui/EpubViewerPage.qml:234
#, kde-format
msgid "Find"
msgstr "Bilatu"

#: content/ui/EpubViewerPage.qml:259
#, kde-format
msgctxt "Book reading progress"
msgid "%1%"
msgstr "% %1"

#: content/ui/EpubViewerPage.qml:279
#, kde-format
msgid "Time left in chapter:"
msgstr "Kapitulua bukatzeko denbora:"

#: content/ui/EpubViewerPage.qml:283
#, kde-format
msgid "Time left in book:"
msgstr "Liburua bukatzeko denbora:"

#: content/ui/EpubViewerPage.qml:294
#, kde-format
msgid "Previous Page"
msgstr "Aurreko orrialdea"

#: content/ui/EpubViewerPage.qml:320
#, kde-format
msgid "Next Page"
msgstr "Hurrengo orrialdea"

#: content/ui/EpubViewerPage.qml:355
#, kde-format
msgid "Loc. %s of %s"
msgstr "Kok. %s / %s"

#: content/ui/EpubViewerPage.qml:356
#, kde-format
msgid "Page %s of %s"
msgstr "Orrialdea %s / %s"

#: content/ui/EpubViewerPage.qml:357
#, kde-format
msgid "Page %s"
msgstr "Orrialdea %s"

#: content/ui/EpubViewerPage.qml:358
#, kde-format
msgid "Close"
msgstr "Itxi"

#: content/ui/EpubViewerPage.qml:360
#, kde-format
msgid "Footnote"
msgstr "Oin-oharra"

#: content/ui/EpubViewerPage.qml:361
#, kde-format
msgid "Go to Footnote"
msgstr "Joan oin-oharrera"

#: content/ui/EpubViewerPage.qml:362
#, kde-format
msgid "Endnote"
msgstr "Amaiera-oharra"

#: content/ui/EpubViewerPage.qml:363
#, kde-format
msgid "Go to Endnote"
msgstr "Joan amaiera-oharrera"

#: content/ui/EpubViewerPage.qml:364
#, kde-format
msgid "Note"
msgstr "Oharra"

#: content/ui/EpubViewerPage.qml:365
#, kde-format
msgid "Go to Note"
msgstr "Joan oharrera"

#: content/ui/EpubViewerPage.qml:366
#, kde-format
msgid "Definition"
msgstr "Definizioa"

#: content/ui/EpubViewerPage.qml:367
#, kde-format
msgid "Go to Definition"
msgstr "Joan definiziora"

#: content/ui/EpubViewerPage.qml:368
#, kde-format
msgid "Bibliography"
msgstr "Bibliografia"

#: content/ui/EpubViewerPage.qml:369
#, kde-format
msgid "Go to Bibliography"
msgstr "Joan bibliografiara"

#: content/ui/GridBrowserDelegate.qml:146
#, kde-format
msgctxt "should be keep short, inside a label. Will be in uppercase"
msgid "New"
msgstr "Berria"

#: content/ui/LibraryPage.qml:19
#, kde-format
msgid "Library"
msgstr "Liburutegia"

#: content/ui/LibraryPage.qml:98
#, kde-format
msgctxt "@action:inmenu"
msgid "Book Details"
msgstr "Liburuaren xehetasunak"

#: content/ui/LibraryPage.qml:110
#, kde-format
msgctxt "@info placeholder"
msgid "Add some books"
msgstr "Gehitu liburu batzuk"

#: content/ui/main.qml:18 content/ui/main.qml:302 main.cpp:65
#, kde-format
msgid "Arianna"
msgstr "Arianna"

#: content/ui/main.qml:203
#, kde-format
msgctxt "Switch to the listing page showing the most recently read books"
msgid "Home"
msgstr "Etxea"

#: content/ui/main.qml:207
#, kde-format
msgid "Home"
msgstr "Etxea"

#: content/ui/main.qml:210
#, kde-format
msgctxt "Switch to the listing page showing the most recently discovered books"
msgid "Recently Added Books"
msgstr "Azkenaldian gehitutako liburuak"

#: content/ui/main.qml:216
#, kde-format
msgctxt ""
"Open a book from somewhere on disk (uses the open dialog, or a drilldown on "
"touch devices)"
msgid "Open Other..."
msgstr "Ireki beste..."

#: content/ui/main.qml:224
#, kde-format
msgctxt "Open the settings page"
msgid "Settings"
msgstr "Ezarpenak"

#: content/ui/main.qml:233
#, kde-format
msgctxt ""
"Heading for switching to listing page showing items grouped by some "
"properties"
msgid "Group By"
msgstr "Taldekatu honen arabera"

#: content/ui/main.qml:236
#, kde-format
msgctxt "Switch to the listing page showing items grouped by author"
msgid "Author"
msgstr "Egilea"

#: content/ui/main.qml:242
#, kde-format
msgctxt "Switch to the listing page showing items grouped by series"
msgid "Series"
msgstr "Sailak"

#: content/ui/main.qml:244
#, kde-format
msgctxt "Title of the page with books grouped by what series they are in"
msgid "Group by Series"
msgstr "Taldekatu sailen arabera"

#: content/ui/main.qml:248
#, kde-format
msgctxt "Switch to the listing page showing items grouped by publisher"
msgid "Publisher"
msgstr "Argitaratzailea"

#: content/ui/main.qml:254
#, kde-format
msgctxt "Switch to the listing page showing items grouped by genres"
msgid "Keywords"
msgstr "Gako-hitzak"

#: content/ui/main.qml:256
#, kde-format
msgctxt "Title of the page with books grouped by genres"
msgid "Group by Genres"
msgstr "Taldekatu generoen arabera"

#: content/ui/main.qml:334 content/ui/SettingsPage.qml:16
#, kde-format
msgid "Settings"
msgstr "Ezarpenak"

#: content/ui/main.qml:342
#, kde-format
msgctxt "@action:button"
msgid "Add Books…"
msgstr "Gehitu liburua…"

#: content/ui/main.qml:363
#, kde-format
msgid "Please choose files"
msgstr "Mesedez aukeratu fitxategiak"

#: content/ui/SettingsPage.qml:19
#, kde-format
msgid "Appearance"
msgstr "Itxura"

#: content/ui/SettingsPage.qml:24
#, kde-format
msgid "Maximum width:"
msgstr "Gehienezko zabalera:"

#: content/ui/SettingsPage.qml:38
#, kde-format
msgid "Margin:"
msgstr "Marjina:"

#: content/ui/SettingsPage.qml:53
#, kde-format
msgid "Show progress"
msgstr "Erakutsi aurrerapena"

#: content/ui/SettingsPage.qml:64
#, kde-format
msgid "Font"
msgstr "Letra-tipoa"

#: content/ui/SettingsPage.qml:72 content/ui/SettingsPage.qml:194
#, kde-format
msgid "Change default font"
msgstr "Aldatu letra-tipo lehenetsia"

#: content/ui/SettingsPage.qml:77
#, kde-format
msgid "Use publisher font"
msgstr "Erabili argitaratzailearen letra-tipoa"

#: content/ui/SettingsPage.qml:88
#, kde-format
msgid "Text flow"
msgstr "Testu-jarioa"

#: content/ui/SettingsPage.qml:94
#, kde-format
msgid "Justify text"
msgstr "Justifikatu testua"

#: content/ui/SettingsPage.qml:107
#, kde-format
msgid "Hyphenate text"
msgstr "Marratxoa duen testua"

#: content/ui/SettingsPage.qml:119
#, kde-format
msgid "Line height:"
msgstr "Lerroaren altuera:"

#: content/ui/SettingsPage.qml:136
#, kde-format
msgid "Brightness:"
msgstr "Distira:"

#: content/ui/SettingsPage.qml:154
#, kde-format
msgid "Colors"
msgstr "Koloreak"

#: content/ui/SettingsPage.qml:159
#, kde-format
msgid "Invert colors"
msgstr "Alderantzikatu koloreak"

#: content/ui/SettingsPage.qml:177
#, kde-format
msgid "About Arianna"
msgstr "«Arianna»ri buruz"

#: content/ui/SettingsPage.qml:185
#, kde-format
msgid "About KDE"
msgstr "KDEri buruz"

#: content/ui/TableOfContentDrawer.qml:23
#, kde-format
msgctxt "@info:tooltip"
msgid "Open table of contents"
msgstr "Ireki edukien aurkibidea"

#: content/ui/TableOfContentDrawer.qml:24
#, kde-format
msgctxt "@info:tooltip"
msgid "Close table of contents"
msgstr "Itxi edukien aurkibidea"

#: content/ui/TableOfContentDrawer.qml:45
#, kde-format
msgctxt "@info:title"
msgid "Table of Contents"
msgstr "Edukien aurkibidea"

#: main.cpp:67
#, kde-format
msgid "EPub Reader"
msgstr "EPub irakurlea"

#: main.cpp:69
#, kde-format
msgid "2022 Niccolò Venerandi <niccolo@venerandi.com>"
msgstr "2022 Niccolò Venerandi <niccolo@venerandi.com>"

#: main.cpp:70
#, kde-format
msgid "Niccolò Venerandi"
msgstr "Niccolò Venerandi"

#: main.cpp:70 main.cpp:71
#, kde-format
msgid "Maintainer"
msgstr "Arduraduna"

#: main.cpp:71
#, kde-format
msgid "Carl Schwan"
msgstr "Carl Schwan"

#: main.cpp:72
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Iñigo Salvador Azurmendi"

#: main.cpp:72
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "xalba@ni.eus"

#: main.cpp:86
#, kde-format
msgid "Epub reader"
msgstr "Epub irakurlea"

#: main.cpp:87
#, kde-format
msgid "Epub file to open"
msgstr "Ireki beharreko «ePub» fitxategia"

#~ msgid "Book details"
#~ msgstr "Liburuaren xehetasunak"

#~ msgid "Configure"
#~ msgstr "Konfiguratu"

#~ msgctxt ""
#~ "Title of the page with books grouped by keywords, character or genres"
#~ msgid "Group by Keywords, Characters and Genres"
#~ msgstr "Taldekatu Gako-hitzen, Pertsonaien eta Generoen arabera"

#~ msgid "arianna"
#~ msgstr "arianna"

#~ msgid "Mastodon client"
#~ msgstr "Mastodon bezeroa"
